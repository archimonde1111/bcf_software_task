#pragma once
#include <iostream>
#include <thread>
#include <vector>
#include <map>
#include <utility>
#include <atomic>
#include <mutex>

//class TestingUnit;

template<class MethodOwner, typename Arg>
class MultithreadingQueue
{
	typedef void (MethodOwner::*FunctionPtr) (Arg);
	
	struct FCriticalData
	{
		// Created because for some reason erasing 0 elementh of vector caused my objects to change variable members...
		unsigned int QueueIndex = 0;
		std::vector<FunctionPtr> ToDoQueue;
		std::vector<std::pair<MethodOwner&, Arg>> ToDoQueueMap;
	};
	FCriticalData CriticalData;
	std::mutex CriticalDataMutex;


	int NumberOfThreadsToExecuteOn = 1;

private:
	void ThreadLoop()
	{
		while(CriticalData.QueueIndex < CriticalData.ToDoQueue.size() && CriticalData.QueueIndex < CriticalData.ToDoQueueMap.size())
		{
			if(CriticalDataMutex.try_lock())
			{
				auto& Function = CriticalData.ToDoQueue[CriticalData.QueueIndex];
				//CriticalData.ToDoQueue.erase(CriticalData.ToDoQueue.begin());
				std::pair<MethodOwner&, Arg> Pair(CriticalData.ToDoQueueMap[CriticalData.QueueIndex]);
				//CriticalData.ToDoQueueMap.erase(CriticalData.ToDoQueueMap.begin());
				CriticalData.QueueIndex++;

				CriticalDataMutex.unlock();

				((Pair.first).*Function)(Pair.second);
				std::cout << "Test created" << (int)Pair.second << std::endl;
			}
		}
	}

public:
	MultithreadingQueue(int inNumberOfThreadsToExecuteOn) 
	{
		NumberOfThreadsToExecuteOn = inNumberOfThreadsToExecuteOn;
	}
	void AddFunctionToQueue(FunctionPtr inFunctionPtr, MethodOwner& OwnerInstance, Arg arg)
	{
		CriticalData.ToDoQueue.push_back(inFunctionPtr);
		std::pair<MethodOwner&, Arg> Pair(OwnerInstance, arg);
		CriticalData.ToDoQueueMap.push_back(Pair);
	}


	void ExecuteQueue()
	{
		std::vector<std::thread> Threads;
		for (int ThreadIndex = 0; ThreadIndex < NumberOfThreadsToExecuteOn; ThreadIndex++)
		{
			std::thread Thread(&MultithreadingQueue::ThreadLoop, this);
			Threads.push_back(std::move(Thread));
		}


		for(std::thread& Thread : Threads)
		{
			Thread.join();
		}
	}
};

