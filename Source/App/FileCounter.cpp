
#include "FileCounter.h"

#include <string>
#include <fstream>


void FileCounter::CountAllFilesInDirectory(const std::string& Path, long long& OutNumberOfDirectories, long long& OutNumberOfFiles, long long& OutNumberOfLinesInFiles)
{
	if(!IsPathValid(Path))
	{
		cout << "WARNING: Passed path to CountAllFilesInDirectory is invalid" << endl;
		return;
	}

	OutNumberOfDirectories = 0;
	OutNumberOfFiles = 0;
	OutNumberOfLinesInFiles = 0;
	const FileSystem::recursive_directory_iterator DirectoryIterator = FileSystem::recursive_directory_iterator(Path);

	for (const FileSystem::directory_entry& File : DirectoryIterator)
	{
		if (File.is_directory())
		{
			OutNumberOfDirectories++;
		}
		else
		{
			OutNumberOfFiles++;
			OutNumberOfLinesInFiles += CountLinesInFile(File);
		}
	}
	return;
}
bool FileCounter::IsPathValid(const std::string& Path)
{
	bool bExists = false;

	FileSystem::path PathToDir = FileSystem::path(Path);
	bExists = FileSystem::exists(PathToDir);

	if (!bExists)
	{
		cout << "Please specify path correctly" << endl;
		return false;
	}
	return true;
}
long long FileCounter::CountLinesInFile(const FileSystem::directory_entry& File)
{
	long long NumberOfLinesInFiles = 0;

	std::ifstream TextFile(File);
	std::string Line;

	while (std::getline(TextFile, Line))
	{
		NumberOfLinesInFiles++;
	}
	return NumberOfLinesInFiles;
}