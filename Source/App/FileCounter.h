#pragma once
#include <iostream>

#include <filesystem>

using std::cout;
using std::cin;
using std::endl;

namespace FileSystem = std::filesystem;

class FileCounter
{
public:
	static bool IsPathValid(const std::string& Path);
	static void CountAllFilesInDirectory(const std::string& Path, long long& OutNumberOfDirectories, long long& OutNumberOfFiles, long long& OutNumberOfLinesInFiles);
	static long long CountLinesInFile(const FileSystem::directory_entry& File);
};