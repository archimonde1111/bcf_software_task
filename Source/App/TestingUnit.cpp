#include "TestingUnit.h"
#include <string>
#include <cmath>
#include <random>
#include <fstream>  

using std::string;

TestingUnit::TestingUnit(const FileSystem::path& inPathToDirectory, const string& TestingFolderName, bool inbRandomizeNumberOfDirectories) 
	: bRandomizeNumberOfDirectories(inbRandomizeNumberOfDirectories), TestingFolderName(TestingFolderName)
{
	SetupTestingUnit(inPathToDirectory);
}
void TestingUnit::SetupTestingUnit(const FileSystem::path& inPathToDirectory)
{
	// Clear path so directories won't be named BCF_Software...Directory_0 itd.
	string TempStringPath = inPathToDirectory.string();
	while (TempStringPath[TempStringPath.length() - 1] != '\\')
	{
		TempStringPath.pop_back();
	}
	PathToDirectory = TempStringPath;

	PathToDirectory += TestingFolderName;
	if(FileSystem::exists(PathToDirectory))
	{
		FileSystem::remove_all(PathToDirectory);
	}
	bool bCreated = FileSystem::create_directory(PathToDirectory);

	PathToDirectory += "\\\\";
}

TestingUnit::TestingUnit(const TestingUnit& inTestingUnit) 
	: bRandomizeNumberOfDirectories(inTestingUnit.bRandomizeNumberOfDirectories), TestingFolderName(inTestingUnit.TestingFolderName)
{
	TotalNumberOfDirectories = inTestingUnit.TotalNumberOfDirectories;
	TotalNumberOfTextFiles = inTestingUnit.TotalNumberOfTextFiles;
	TotalNumberOfLinesInTextFiles = inTestingUnit.TotalNumberOfLinesInTextFiles;
	TotalNumberOfLettersInTextFiles = inTestingUnit.TotalNumberOfLettersInTextFiles;

	PathToDirectory = inTestingUnit.PathToDirectory;
}

// #TODO:(3) Add new parameter that will define depth of directory "tree"
void TestingUnit::InitializeTest(ETestRange TestRange)
{
	if(bRandomizeNumberOfDirectories)
	{
		int PreviousTestRange = (int)TestRange;
		PreviousTestRange--;
		LL MinNumberOfDirectories = TenToPower(PreviousTestRange);
		LL MaxNumberOfDirectories = TenToPower((int)TestRange);
		TotalNumberOfDirectories = GetRandomNumber(MinNumberOfDirectories, MaxNumberOfDirectories);
	}
	else
	{
		TotalNumberOfDirectories = TenToPower((int)TestRange);
	}

	string PathToNextFile = PathToDirectory.string() += "Directory_";

	for(LL DirectoryIndex = 0; DirectoryIndex < TotalNumberOfDirectories; DirectoryIndex++)
	{
		PathToNextFile += std::to_string(DirectoryIndex);

		FileSystem::create_directory(PathToNextFile);

		string FileName = "TextFile_" + std::to_string(DirectoryIndex) + ".txt";
		AddTextFile(PathToNextFile, FileName);

		while(PathToNextFile[PathToNextFile.length() -1] != '_')
		{
			PathToNextFile.pop_back();
		}
	}
}
void TestingUnit::AddTextFile(const string& PathToFileDirectory, const string& FileName)
{
	std::ofstream NewFile (PathToFileDirectory + "\\\\" + FileName);
	TotalNumberOfTextFiles++;

	AddLinesToFile(NewFile, 1, 10);

	NewFile.close();
}
void TestingUnit::AddLinesToFile(std::ofstream& File, const LL& NumberOfLinesToAdd, const LL& NumberOfLettersPerLine)
{
	for(LL LineIndex = 0; LineIndex < NumberOfLinesToAdd; LineIndex++)
	{
		for(LL LetterIndex = 0; LetterIndex < NumberOfLettersPerLine; LetterIndex++)
		{
			File << 'a';
			TotalNumberOfLettersInTextFiles++;
		}
		File << std::endl;
		TotalNumberOfLinesInTextFiles++;
	}
}


LL TestingUnit::TenToPower(const int& inPower) const
{
	LL Number = 10;
	for(LL Index = 1; Index < inPower; Index++)
	{
		Number *= 10;
	}
	return Number;
}
LL TestingUnit::GetRandomNumber(const LL& MinValue, const LL& MaxValue)
{
	// Get seed from hardware
	std::random_device RandomSeed;
	// random-number engine used
	std::mt19937 RandomNumber(RandomSeed());
	// guaranteed unbiased
	std::uniform_int_distribution<LL> uni(MinValue, MaxValue);

	return uni(RandomNumber);
}