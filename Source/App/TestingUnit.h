#pragma once

#include <iostream>
#include <filesystem>

namespace FileSystem = std::filesystem;

using LL = long long;

// Scope defines how huge number of directories should be created
// #NOTE: these numbers are powers of 10 in InitializeTest method
enum class ETestRange
{
	TS_ReallySmall = 2,
	TS_Small = 3,
	TS_Medium = 4,
	TS_High = 5,
	TS_Mega = 6,
	TS_WTF = 7
};

class TestingUnit
{
private:
	// #TODO: #CONSIDER: What if LL is not large enough?
	LL TotalNumberOfDirectories = 0;
	LL TotalNumberOfTextFiles = 0;
	LL TotalNumberOfLinesInTextFiles = 0;
	LL TotalNumberOfLettersInTextFiles = 0;

	const std::string TestingFolderName;
	const bool bRandomizeNumberOfDirectories = false;

	FileSystem::path PathToDirectory;

private:
	// Prepares Testing folder for files and changes path to lead to this folder
	void SetupTestingUnit(const FileSystem::path& inPathToDirectory);
	void AddTextFile(const std::string& PathToFileDirectory, const std::string& FileName);
	void AddLinesToFile(std::ofstream& File, const LL& NumberOfLinesToAdd, const LL& NumberOfLettersPerLine);

private:
	LL TenToPower(const int& inPower) const;
	LL GetRandomNumber(const LL& MinValue, const LL& MaxValue);

public:
	const LL& GetTotalNumberOfDirectories() { return TotalNumberOfDirectories; }
	const LL& GetTotalNumberOfDirectories() const { return TotalNumberOfDirectories; }
	const LL& GetTotalNumberOfTextFiles() { return TotalNumberOfTextFiles; }
	const LL& GetTotalNumberOfTextFiles() const { return TotalNumberOfTextFiles; }
	const LL& GetTotalNumberOfLinesInTextFiles() { return TotalNumberOfLinesInTextFiles; }
	const LL& GetTotalNumberOfLinesInTextFiles() const { return TotalNumberOfLinesInTextFiles; }
	const LL& GetTotalNumberOfLettersInTextFiles() { return TotalNumberOfLettersInTextFiles; }
	const LL& GetTotalNumberOfLettersInTextFiles() const { return TotalNumberOfLettersInTextFiles; }

	const FileSystem::path& GetPathToDirectory() { return PathToDirectory; };
	const FileSystem::path& GetPathToDirectory() const { return PathToDirectory; };

public:
	TestingUnit() = delete;
	// @param inPathToDirectory is path to directory in which Testing folder should be created
	// #NOTE: If Testing folder already exists in there it will be deleted
	TestingUnit(const FileSystem::path& inPathToDirectory, const std::string& TestingFolderName, bool inbRandomizeNumberOfDirectories);
	TestingUnit(const TestingUnit& inTestingUnit);

	// #TODO:(1) Add clearing of Testing folder if one test has already been done.
	// Will create given number of Directories,Files and lines in Testing folder
	void InitializeTest(ETestRange TestRange);


	TestingUnit& operator=(TestingUnit& inTestingUnit)
	{
		if(this == &inTestingUnit)
		{
			return *this;
		}
		
		TotalNumberOfDirectories = inTestingUnit.TotalNumberOfDirectories;
		TotalNumberOfTextFiles = inTestingUnit.TotalNumberOfTextFiles;
		TotalNumberOfLinesInTextFiles = inTestingUnit.TotalNumberOfLinesInTextFiles;
		TotalNumberOfLettersInTextFiles = inTestingUnit.TotalNumberOfLettersInTextFiles;

		PathToDirectory = inTestingUnit.PathToDirectory;

		return *this;
	}
};

