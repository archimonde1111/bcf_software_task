
#include "BCF Software zadanie.h"

#include "App/FileCounter.h"
#include "App/TestingUnit.h"
#include "App/MultithreadingQueue.h"


int main(int argc, char* argv[])
{
	// #TODO:(2) Handle using arguments passed from command line
	if(argc == 1)
	{
		std::vector<TestingUnit> TestingUnits;
		MultithreadingQueue<TestingUnit, ETestRange> TestingUnitsQueue(2);

		cout << "Do you want to run tests?(Y/N)" << endl;
		char Answear;
		cin >> Answear;
		if(Answear == 'Y' || Answear == 'y')
		{
			TestingUnits.push_back(TestingUnit(argv[0], "Testing1", false));
			TestingUnits.push_back(TestingUnit(argv[0], "Testing2", false));
			TestingUnits.push_back(TestingUnit(argv[0], "Testing3", false));

			TestingUnitsQueue.AddFunctionToQueue(&TestingUnit::InitializeTest, TestingUnits[0], ETestRange::TS_Medium);
			TestingUnitsQueue.AddFunctionToQueue(&TestingUnit::InitializeTest, TestingUnits[1], ETestRange::TS_Small);
			TestingUnitsQueue.AddFunctionToQueue(&TestingUnit::InitializeTest, TestingUnits[2], ETestRange::TS_Small);

			TestingUnitsQueue.ExecuteQueue();

			cout << "TestingUnits created" << endl;

			for(const TestingUnit& TestingUnit : TestingUnits)
			{
				LL NumberOfDirectories = 0;
				LL NumberOfFiles = 0;
				LL NumberOfLines = 0;
				FileCounter::CountAllFilesInDirectory(TestingUnit.GetPathToDirectory().string(), NumberOfDirectories, NumberOfFiles, NumberOfLines);

				bool bPassed = false;
				bPassed = TestingUnit.GetTotalNumberOfDirectories() == NumberOfDirectories;
				cout << "Number of directories " << NumberOfDirectories << ' ' << bPassed << endl;

				bPassed = TestingUnit.GetTotalNumberOfTextFiles() == NumberOfFiles;
				cout << "Number of files " << NumberOfFiles << ' ' << bPassed << endl;

				bPassed = TestingUnit.GetTotalNumberOfLinesInTextFiles() == NumberOfLines;
				cout << "Number of lines " << NumberOfLines << ' ' << bPassed << endl << endl;
			}
		}
		else
		{
			LL NumberOfDirectories = 0;
			LL NumberOfFiles = 0;
			LL NumberOfLines = 0;

			cout << "Specify path to directory you want to check" << endl;
			std::string Path;
			cin >> Path;

			while(!FileCounter::IsPathValid(Path))
			{
				cin >> Path;
			}

			FileCounter::CountAllFilesInDirectory(Path, NumberOfDirectories, NumberOfFiles, NumberOfLines);

			cout << "Number of directories " << NumberOfDirectories << endl;
			cout << "Number of files " << NumberOfFiles << endl;
			cout << "Number of lines " << NumberOfLines << endl;
		}
	}
	
	std::cin.get();
	return 0;
}

